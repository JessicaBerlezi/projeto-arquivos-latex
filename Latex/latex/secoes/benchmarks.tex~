\section{Benchmarks} \label{sec:benchmarks}

%Para mensurar o desempenho de um sistema é analisado a quantidade de instruções por segundo (IPS) que este sistema é capaz de executar. Porém, em função do aumento do poder computacional dos sistemas, atualmente tem-se adotado como base o múltiplo desta unidade, o MIPS, este que representa milhões de instruções por segundo. Esta unidade representa o desempenho máximo, ou de pico que um sistema pode operar.\\

%Com a evolução e o surgimento de novas arquiteturas de computadores, torna-se cada vez mais difícil mensurar a performance destes novos sistemas. Assim, programas denominados de \textit{benchmark} tem sido propostos e utilizados, sempre almejando mensurar e padronizar o desempenho.\\

Para comparar sistemas computacionais distintos é preciso que eles estejam rodando algum tipo de programa, esses programas usados para comparação de sistemas são denominados benchmarks. Idealmente os melhores programas para testes seriam aplicações normais que serão executadas neles, infelizmente essas aplicações não são facilmente portáveis para outros sistemas e os testes são difíceis de serem repetidos comprometendo a comparação dos sistemas. Em vez de usar aplicações reais usamos testes sintéticos que são desenvolvidos modelando as características principais de aplicações reais, além de serem facilmente portados e simples de serem executados~\cite{lilja2000measuring}.

Linpack é um benchmark que obteve bastante sucesso, foi desenvolvido por Jack Dongarra e já foi utilizado em uma grande variedade de sistemas~\cite{dongarra1979linpack}. Esse benchmark consiste em resolver um sistema de equações lineares denso utilizando fatoração LU, possui uma grande quantidade de adições e multiplicações de ponto flutuante e seu principal resultado é a quantidade de operações de ponto flutuante por segundo FLOPS

Inicialmente ele utilizava matrizes de tamanho fixo da ordem de 100 x 100, após algumas versões e com a necessidade de avaliar o desempenho dos sistemas de memória distribuída foi desenvolvido o High-Performance Linpack (HPL), versão essa que é utilizada até hoje e sua principal aplicação é na construção da lista dos 500 sistemas mais rápidos, chamada de TOP500, atualmente o sistema mais rápido é capaz de executar até 8 PFLOPS.

%O benchmark Linpack é composto por um pacote de funções que são utilizadas para a solução de sistemas de equações lineares densos. Foi desenvolvido por Jack Dongarra para avaliar o desempenho de computadores. Em sua primeira versão, disponibilizada em 1979, era denominado de Linpack-100 pelo fato de utilizar matrizes de ordem 100 x 100.\\ 


%Anos depois, este tamanho fixo para a matriz de 100 x 100, o tornou inviável pelo fato de que essa matriz passou a caber na memória cache dos microprocessadores da época. Para resolver esta questão, uma nova versão Linpack  foi criada com matrizes de ordem 1000, passando então a ser demoninado de Linpack-1000. \\

%Porém, com a necessidade de avaliar o desempenho dos sistemas de memória distribuída ele novamente foi atualizado. Suge assim o High-Performance Linpack (HPL), versão que até hoje é utilizada. Nesta versão o usuário pode definir o tamanho da matriz, bem como outras configurações para alcançar o melhor desempenho possível em sua arquitetura. \\


%Originalmente era um pacote de subrotinas que tinha por finalidade resolver sistemas de equações lineares algébricas. Atualmente utiliza-se de rotinas algébricas para medir o tempo necessário para a resolução de um sistema denso de equações lineares de precisão dupla (64 bits), utilizando para isto o conceito de um ambiente de memória distribuída.  Seu funcionamento concentra-se em dois conjuntos de rotinas: um para decomposição de matrizes e outro para resolver o sistema de equações lineares resultantes da decomposição~\cite{dongarra1979linpack}.\\


%Em sua execução, o benchmark  mede o desempenho das duas rotinas denominadas de DGEFA e DGESL. Estas rotinas trabalham com ponto flutuante de dupla precisão, já as rotinas SGEFA e SGESL trabalham com expressões de ponto flutuante de precisão simples. A rotina DGEFA realiza a decomposição LU com pivotamento parcial, e a rotina DGESL usa este tipo de decomposição para resolver o sistema de equações lineares~\cite{linpack}.\\
